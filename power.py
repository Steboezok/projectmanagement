def power(a, n):
    if n == 0:
        return 1
    else:
        return a * power(a, n - 1)

a = int(input("Введите вещественное число a: "))
n = int(input("Введите натуральное число n: "))
result = power(a, n)
print(f"Число {a} в степени {n} равно {result}")
