class Student:
    def __init__(self, full_name, birth_date, marks):
        self.full_name = full_name
        self.birth_date = birth_date
        self.marks = marks


def get_students():
    num_students = int(input("Введите количество студентов в группе: "))
    students = []

    for i in range(num_students):
        full_name = input(f"Введите ФИО студента {i+1}: ")
        birth_date = input(f"Введите дату рождения студента {i+1} (гггг-мм-дд): ")
        num_subjects = int(input("Введите количество предметов в зачетке студента: "))
        marks = {}

        for j in range(num_subjects):
            subject = input(f"Введите предмет {j+1}: ")
            exam_date = input(f"Введите дату экзамена по предмету {subject} (гггг-мм-дд): ")
            mark = float(input(f"Введите оценку по предмету {subject}: "))
            teacher = input(f"Введите ФИО преподавателя по предмету {subject}: ")
            marks[subject] = {"Дата экзамена": exam_date, "ФИО преподавателя": teacher, "Оценка": mark}

        student = Student(full_name, birth_date, marks)
        students.append(student)

    return students


def get_student_with_lowest_grades(students):
    lowest_grades = float("inf")
    student_with_lowest_grades = None

    for student in students:
        avg_grade = sum(mark["Оценка"] for mark in student.marks.values()) / len(student.marks)

        if avg_grade < lowest_grades:
            lowest_grades = avg_grade
            student_with_lowest_grades = student

    return student_with_lowest_grades


# Получаем список студентов
student_list = get_students()

# Находим студента с самой низкой успеваемостью
student_lowest_grades = get_student_with_lowest_grades(student_list)

# Выводим информацию о студенте с самой низкой успеваемостью
print("Студент с самой низкой успеваемостью:")
print("ФИО:", student_lowest_grades.full_name)
print("Предметы и оценки:", student_lowest_grades.marks)
